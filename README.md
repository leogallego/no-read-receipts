Forked from: https://github.com/stephenlewis/no-read-receipts

Created by: Stephen Lewis

This extension has been modified to work in Mozilla Firefox.

I could not find an adequate solution to auto declining Gmail read requests in Firefox.  A working plugin existed for Chrome, but needed a slight modification to work for Firefox.

Modified by: Papaya
